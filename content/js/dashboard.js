/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9147939305265581, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.5765687967760507, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9998630962159795, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.9434112646121148, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.679642166344294, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.07891246684350132, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.8212538226299694, 500, 1500, "me"], "isController": false}, {"data": [0.816847479259732, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.5355093256814921, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.9404877104702922, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.4261213720316623, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 60829, 0, 0.0, 246.0485459238188, 10, 4583, 46.0, 796.0, 1133.0, 1675.9700000000048, 201.98904200564502, 858.7388059314296, 233.94753055993692], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 1737, 0, 0.0, 862.8664363845714, 207, 2229, 891.0, 1223.2, 1317.1999999999998, 1545.8199999999988, 5.7741979449573, 3.648345771862669, 7.1444421838485335], "isController": false}, {"data": ["getLatestMobileVersion", 36522, 0, 0.0, 40.65300366902163, 10, 1035, 26.0, 75.0, 113.95000000000073, 193.0, 121.75664007414346, 81.44853364334793, 89.77174146091633], "isController": false}, {"data": ["findAllConfigByCategory", 5646, 0, 0.0, 264.8251859723696, 29, 1372, 211.0, 525.0, 666.0, 920.5899999999992, 18.80038493172124, 21.260591553645696, 24.41846871014575], "isController": false}, {"data": ["getNotifications", 2068, 0, 0.0, 724.75, 68, 1603, 743.5, 1235.0, 1320.1, 1486.62, 6.876623139105307, 57.168645296097154, 7.648900151797799], "isController": false}, {"data": ["getHomefeed", 754, 0, 0.0, 1991.037135278513, 747, 4583, 1948.5, 2614.0, 2820.25, 3365.6500000000015, 2.5037356798937407, 30.50694343558028, 12.528458616968289], "isController": false}, {"data": ["me", 3270, 0, 0.0, 458.08379204893004, 69, 1589, 362.0, 880.9000000000001, 992.0, 1139.4499999999998, 10.876796168174561, 14.310876380388505, 34.467972232154736], "isController": false}, {"data": ["findAllChildrenByParent", 3134, 0, 0.0, 477.82992980216994, 56, 1610, 356.0, 990.0, 1075.25, 1266.9000000000005, 10.421550729408793, 11.703889979316513, 16.650055657532015], "isController": false}, {"data": ["getAllClassInfo", 1394, 0, 0.0, 1076.0014347202284, 85, 1773, 1188.5, 1442.5, 1530.0, 1670.4999999999995, 4.63390797337996, 13.296378917962542, 11.897015685562417], "isController": false}, {"data": ["findAllSchoolConfig", 5167, 0, 0.0, 289.3168182697894, 39, 1337, 244.0, 542.1999999999998, 686.0, 926.0, 17.21180142637766, 375.3651850135409, 12.623108272665647], "isController": false}, {"data": ["getChildCheckInCheckOut", 1137, 0, 0.0, 1317.354441512753, 475, 2107, 1315.0, 1549.2, 1639.3999999999996, 1907.4799999999996, 3.7808377698045073, 252.0866198568386, 17.397761300116052], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 60829, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
